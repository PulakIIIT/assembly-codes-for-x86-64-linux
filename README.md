To run the codes use the following command-

```sh
gcc -c -nostdlib <filename.s>
```

And then run with-
```sh
gdb ./a.out
```