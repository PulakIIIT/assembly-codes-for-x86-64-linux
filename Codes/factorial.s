
.global _start

.text   # aim to calculate factorial through recursion 
        
# r12 used as safe regsiter that callee cannot alter
_start:
    mov $5, %rdi    # argument to function is stored in %rdi
    mov $0, %rax    # rec will return value to %rax by convention
    mov $0, %r12    # r12 = 0, not required just for good measure
    call rec
    jmp exit


rec:
    push %r12         # make backup of %r12 to the stack  
    mov %rdi, %r12    # saved value of argument i.e n to safe register like %r12
    cmp $1, %rdi      # if n == 1 return 1
    jz end1           
    sub $1, %rdi      # n = n - 1            
    call rec          # rax = rec(n - 1)
    imul %r12, %rax   # rax = rec(n - 1) * n
    pop %r12          # restore %r12 for the caller assumption
    ret

end1:                  # base case when n == 1
    mov $1, %rax       # return value is 1
    pop %r12           # restore value of safe register
    ret


exit:
    mov     $60, %rax               # system call 60 is exit
    xor     %rdi, %rdi              # we want return code 0
    syscall
