# created by Pulak Malhotra on 15th Feb 2020
.global _start

.text   # aim to calculate gcd of two positive numbers through recursion 

# r9 is the first argument and r10 is the second argument
# assume rbp, r11 as safe register which needs to be preserved
# return value is stored in rcx (also the final answer)
# note we do not safe registers in this example because 
# value of a or b is not required after the recursive call
# but i am just using it to demonstrate the concept

_start:
    mov $54, %r9    # a = 12
    mov $729, %r10  # b = 4 
    mov $0, %rcx    # initialize answer to be 0
    call gcd        # rax = gcd(a, b)
    jmp exit

gcd:
    push %rbp         # backup rbp
    push %r11         # backup r11
    mov %r9, %rbp     # save a in safe register(rbp)
    mov %r10, %r11    # save b in safe register(r11)
    cmp $0, %r10      # if b == 0
    jz base           # base case
                      # prepare for a % b  
    mov %r10, %r12    # backup b to r12 
    mov $0, %rdx      # rdx = 0
    mov %r9, %rax     # rax = a
    idiv %r10         # rdx = a % b 
    mov %rdx, %r10    # b = a % b
    mov %r12, %r9     # a = b
    call gcd          # assume returned value is in rax
    pop %r11          # restore r11
    pop %rbp          # restore rbp (note to be done in order) 
    ret

base:
    mov %r9, %rcx     # store answer in rcx
    pop %r11          # restore r11, rbp in order
    pop %rbp
    ret	

exit:
    mov     $60, %rax               # system call 60 is exit
    xor     %rdi, %rdi              # we want return code 0
    syscall
