
.global _start

.text   # aim to calculate nth fibonacci number through recursion 
        
# rax will store return value of a function
# rdi will store argument of the function
# value of register rbx, r11 needs to be preserved by callee
# rbx stores the value of n, r11 stores value of rec(n - 1)

# expected output
# 0 -> 0
# 1 -> 1
# 2 -> 1
# 3 -> 2
# 4 -> 3
# 5 -> 5
# 6 -> 8
# 7 -> 13
# 8 -> 21

_start:
    mov $8, %rdi    # n = 5
    mov $0, %rax    # ans = 0
    mov $0, %rbx    # rbx = 0, not required
    call rec
    jmp exit

rec:
    push %rbx       # backup rbx
    push %r11       # backup r11
    mov %rdi, %rbx  # move n to %rbx
    cmp $1, %rdi    # if n == 1 return 1
    jz end1
    cmp $0, %rdi    # if n <= 0 return 0
    jle end0
    sub $1, %rdi    # n = n - 1
    call rec        # rax = rec(n - 1)
    mov %rax, %r11  # move  rec(n - 1) to safe register
    sub $2, %rbx    # rbx = n - 2
    mov %rbx, %rdi  # n = n - 2
    call rec        # rax = rec(n - 2)
    add %r11, %rax  # rax = rec(n - 1) + rec(n - 2)
    pop %r11        # note it is really important to pop back in order
    pop %rbx        # restore r11 and rbx
    ret

end1:
    mov $1, %rax    # return value is 1
    pop %r11        # pop back in order
    pop %rbx
    ret

end0:
    mov $0, %rax    # return value is 0
    pop %r11        # pop back in order    
    pop %rbx
    ret

exit:
    mov     $60, %rax               # system call 60 is exit
    xor     %rdi, %rdi              # we want return code 0
    syscall
